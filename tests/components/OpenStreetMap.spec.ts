import { shallowMount } from '@vue/test-utils';
import OpenStreetMap from '@/components/OpenStreetMap.vue';

describe('OpenStreetMap', () => {
    const wrapper = shallowMount(OpenStreetMap);

    test('does a wrapper exist', () => {
        expect(wrapper.exists()).toBe(true);
    });
});