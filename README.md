# nantes-bus-front

### Rappels des enjeux
```
L'objectif de ce projet est de développer une petite application qui consomme une API, celles des transports de Nantes.
L'application n'est que la partie visible du projet, le véritable objectif est de réaliser cette application en respectant des processus de qualités.
```
### Rôle des devs dans le projet
```
Elouan : Rédaction du readMe et rédaction des tâches et merge request et développement
Martin : Organisation du gitlab et des pipelines (Organisation) et développement
Maxime : Développement du projet front
```

### Architecture et technologies du projet (lib+ versions) + règles de nommage
```
Architecture : 
Application :
- Une application uniquement cliente qui est la partie que vois l'utilisateur.
- Quand l'utilisateur ouvre l'application, un appel à l'API est effectué est les données récupérés et renvoyés sur l'application.
Système de branches :
- Une branche master qui contient le projet dit (en production)
- Une branche develop qui contient la version de developpement actuel utilisé par les développeurs
- Chaque fonctionnalités ajouté par un développeur est poussé sur une nouvelle branche qui est merge sur la branche develop
Technologies utilisées :
- VueJS 3
- HTML
- CSS
- JS

```

### Points particuliers et risques
```
Risques techniques :
- L'API est publique donc données pouvant être corrompus
Risque humains :
- Manque de connaissances techniques
```

### Définition du Ready et du Done
```
Todo : Tâche non commencé, à faire
Deployed : Tâche terminée et déployé sur la branche principale
```
### Fonctionnement des pipelines
```
Quand une merge request / commit est lancé, les pipelines de builds sont lancés.
Sonarcloud, est lancé dans les pipelines, il analysera le code et remontera des erreurs si les nomages ou erreurs de syntaxe sont détectés
```
## Project initialisation
```
npm install
```

### Local run
```
npm run serve
```

### Compile for production deployment
```
npm run build
```

### Run unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```
