export default class BusStation {

    datasetid: string;

    recordid: string;

    fields: {
        stop_id: string,
        wheelchair_boarding: string,
        stop_coordinates: number[],
        location_type: string,
        stop_name: string,
    };

    geometry: {
        type: string,
        coordinates: number[]
    };

    record_timestamp: string;

}
